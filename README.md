# ISPRA - Internship

## Comparison of return-level estimation (MLE/POT, Bayesian) comparing buoy and satellite data

## Description
This project has 2 main directories:
* Datasets:
    * RData files
    * Satellite Datasets
    * Buoy datasets
* code:
    * Retrieving buoy and satellite data
    * Optimal bounding box selection
    * Exploratory analysis 
    * EVA using MLE/POT for both buoy and satellite
    * EVA using Bayesian model (in Stan) for buoy and satellite

## Usage
To use this repository, you can use the already-made buoy/satellite datasets, or use the R codes to create your own datasets.

## Authors and acknowledgment
I'd like to extend a warm shout-out to my tutors at ISPRA - your assistance was instrumental in bringing this project to life. Also, a huge thanks goes to Professor Jonalasinio, not only for pointing me towards this wonderful internship opportunity but also for your continued guidance throughout the journey.

## Project status
Not finished yet
